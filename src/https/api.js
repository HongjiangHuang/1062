import {vm} from '../main'

//获取设备密码列表通过设备id
export const getPasswordListByDeviceId = function (device_id) {

  //mock
  return new Promise((resolve, reject) => {
    return resolve([
      {
        "create_time": "2018-08-28 18:31:16", // 创建时间
        "dlid": "868744033731161", // 设备ID
        "effective": 0, // 有效类型
        "effective_str": "7天", // 有效日期说明
        "password": "1234567", // 有效密码（若status=更新中，本密码为更新后的密码）
        "status": "正常", // 密码状态：删除中、已删除、冻结中、已冻结、更新中、已更新
        "status_code": 0, // 密码状态码：0正常 1删除中 2已删除 3更新中 4已更新 5冻结／启用中 6已冻结 7已启用
        "type_str": "临时密码" // 密码类型：管家、临时
      }
    ]);
  });

  let access_token = vm.$store.state.rqToken;
  return vm.$https.get('api/device/get_lock_pwd_lists', {
    "access_token": access_token,
    "dlid": device_id,
    "pwd_type": 0,
  }).then(res => {
    console.log(res);
    return res.data.data
  });
};


//获取房间信息
export const getRoomDetailById = function (room_id) {
  return vm.$https.get('api/room/room_detail', {
    "rid": room_id,
  }).then(res => {
    return res.data.data;
  });
};


//解绑房间设备
export const unBindRoomDevice = function (room_id) {
  return getRoomDetailById(room_id).then(function (data) {
    console.log(data);
    return vm.$https.post('api/room/room_edit', {
      'rid': room_id,
      'room_type': data.room_type,
      'aid': data.apartment.aid,
      'price': data.price,
      'space': data.space,
      'floor': data.floor,
      'config': data.config,
      'desc': data.desc,
      'picture': data.picture
    });
  }).catch(function (e) {
    console.error(e);
  });
};


// export const editRoom = function () {
//
//   let access_token = vm.$store.state.rqToken;
//   return vm.$https.post('api/room/room_edit', {
//     "access_token": access_token,
//     "rid": 0, //TODO
//     "dlid": 0, //TODO
//     "name": 0,
//     "room_type": 0,
//     "aid": 0,
//     "price": "",
//     "space": "",
//     "floor": "",
//     "config": ""
//   });
// };
