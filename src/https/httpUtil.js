import store from '../store'
import errcode from '../data/codeMes.json';
//配置请求基地址
const $baseURL = 'https://wx.grandacom.cn/'
const vm = this;
//src/utils/net.js
// import wx from 'wx';//引用微信小程序wx对象
// import { bmobConfig } from '../config/bmob';//bmob配置文件

const net = {
  ajax(token, method, url, data) {
    console.log('http', url);
    //不填为空
    data = data || {};

    if (token) {
      data.access_token = store.state.rqToken
    }

    return new Promise((resolve, reject) => {
      wx.request({
        url: $baseURL + url,
        data: data,
        method: method, // OPTIONS, GET, HEAD, POST, PUT, DELETE, TRACE, CONNECT
        header: {
          // 'X-Bmob-Application-Id': bmobConfig.applicationId,
          // 'X-Bmob-REST-API-Key': bmobConfig.restApiKey,
          'Content-Type': 'application/json'
        },
        success: function (res) {
          if (res.statusCode != 200) {
            wx.showToast({
              title: res.statusCode,
              icon: "none",
              duration: 2500,
              mask: true,
            });
            resolve(res);
          }

          if (res.data.code == 0) {
            return resolve(res);
          }

          for (let i = 0; i < errcode.data.length; i++) {
            // console.log(res);
            // console.log(errcode.data[i].code,res.data.code);
            if (errcode.data[i].code == res.data.c || errcode.data[i].code == res.data.code) {
              //缓存的4001和2001是不跑toast的。所以4001、4005、2001都自己手动toast
              if (res.data.code == -4001 || res.data.code == -4005 || res.data.code == -2001) {
                return resolve(res);
              }
              wx.showToast({
                title: errcode.data[i].msg,
                icon: 'none',
                duration: 2500,
                mask: true,
              });
              resolve(res);
              // console.log(errcode.data[i].code,errcode.data[i].msg);
              break;
            }
            // 遍历完都没有找到code.
            //因为i是逻辑执行完再++所以这里有个+1
            if (i + 1 == errcode.data.length) {
              console.log('未存在表中的错误');
              //特殊的不存在表中的只输出msg的形式
              let $msg = res.data.msg ? res.data.msg + "(" + res.data.code + ")" : "未知错误"
              wx.showToast({
                title: $msg,
                icon: "none",
                duration: 2500,
                mask: true,
              });
              resolve(res);
            }
          }
        },
        fail: function (error) {
          wx.showToast({
            title: "fail",
            icon: "none",
            duration: 2500,
            mask: true,
          });
          reject(error);
          // 这里不处理要搞一个trycatch
        },
      })
    })
  },
  post(url, data) {
    console.log('post', url);
    return this.ajax(1, 'POST', url, data);
  },
  get(url, query) {
    return this.ajax(1, 'GET', url, query);
  }
}

export default net;//暴露出来供其他文件引用
