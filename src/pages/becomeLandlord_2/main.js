import Vue from 'vue'
import App from './index'

const app = new Vue(App)
app.$mount()


export default {
  config: {
    navigationBarTitleText: "上传手持证件照片"
  }
}