import Vue from 'vue'
import App from './App'
import store from './store'
import bus from './utils/bus'
import base from './utils/base'
import code from './data/codeMes'
// import Toast from './utils/component'
// import maxToast from './components/meToast.vue'
import https from './https/httpUtil'

// Vue.use(base);
// Vue.component("max-toast",maxToast)

Vue.config.productionTip = false
App.mpType = 'app'

Vue.prototype.$store=store
Vue.prototype.$url='https://iot.grandacom.cn/'
Vue.prototype.$url2='https://wx.grandacom.cn/'
Vue.prototype.$bus=bus;
Vue.prototype.fns=base;
Vue.prototype.codeMes=code;
Vue.prototype.$https=https
// Vue.prototype.g=function(arg){
//   console.log(arg)
// }

const app = new Vue(App)
app.$mount()

export default {
  // 这个字段走 app.json
  config: {
    // 页面前带有 ^ 符号的，会被编译成首页，其他页面可以选填，我们会自动把 webpack entry 里面的入口页面加进去
    pages: ['^pages/initialize/index/main','pages/index/main'],
    window: {
      backgroundTextStyle: 'light',
      navigationBarBackgroundColor: '#fff',
      navigationBarTitleText: '智乐锁',
      navigationBarTextStyle: 'black',
      // networkTimeout:{
      //   "request":1
      // },
    }
  }
}

export const vm = app;
