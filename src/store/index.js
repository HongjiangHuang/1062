import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

//数据处理需要注意的是，赋值是给一个地址，如果变量改变则会影响vuex里的值，这里涉及到深浅拷贝，一般情况下不进行直接拷贝或者修改，或者先进行深拷贝处理。
export default new Vuex.Store({
	state:{
    //socket x 2
		notifiNum:0,          //通知控制。。。变量num
    mesArr:[],            //这个居然是=的arr
    
    //bluetooth
    // BLEdlid:"",           //正在蓝牙连接的设备id。
    BLEstatus:false,        //蓝牙状态。base的断开和连接所有
                            //蓝牙的dlid就是设备里来的。所以应该是拿statue而不是blueID

    role:0,               //用户组。登录验证最后跟token一起获取的role
		rqToken:"",           //token。登录验证完之后获取到的token
    idCardUrl:"",         //成为房东2 back回去的图片微信地址数据保留
    rid:"",               //房间id。房间列表进入的保留值
    dlid:"",              //设备id。房间详情拿到的设备id
    roomInfo:"",             //房间详情。全部数据？后面拿3个id
    superPsw:"",              //门锁超级管理员密码。deviceInfo获取详情之后传到vuex
    ruid:"",                 //房间详情->租客。寄存的租客id。。。退出或修改之后初始化
    cid:"",                  //合同编号。（租客编辑时生成的租客和合同，合同编辑需要用到）
	},
	mutations:{
    setRole(state,$data){
      state.role=$data;
    },
		setToken(state,$data){
			state.rqToken=$data;
		},
		notifiData(state,$data){
			state.notifiNum=$data.num;
			state.mesArr=$data.arr;
		},
    //手持身份证上传完2张之后返回到的页面。。
		setIdCard(state,$data){
			state.idCardUrl=$data;
    },
    // 首页到房间详情保存 (之后可通过请求详情获取其他)
    setRid(state,$data){
      state.rid=$data;
    },
    setDlid(state,$data){
      state.dlid=$data;
    },
    setBLEstatus(state,$data){
      state.BLEstatus=$data;
    },
    setRommInfo(state,$data){
      state.roomInfo=$data;
    },
    //超级管理员密码
    setSuperPsw(state,$data){
      state.superPsw=$data;
    },
    setRuid(state,$data){
      state.ruid=$data;
    },
    //合同编号
    setCid(state,$data){
      state.cid=$data;
    },
  }
})