// exports.install = function (Vue, options) {
//   Vue.prototype.text1 = function (){//全局函数1
//     console.log('执行成功1');
//   };
//   Vue.prototype.text2 = function (){//全局函数2
//     console.log('执行成功2');
//  };
// };
// import Vue from "vue";
// 20180908 差不多就是为了蓝牙来存放的js了
var utils = {
  //放置蓝牙写入和读取功能的data
  BLEdata:{
    deviceId:"",      //mac...蓝牙连接页传过来保存的值(后台瞎几把要的mac要转)
    serviceId:"",     //服务项操作号
    uuid:"",          //uuid
    category:"",      //兑换的code.保存该值，决定emit哪个会响应执行
    commandId:"",     //comID
    imac:"",          //只有是iphone才会有值。这个应该不用初始
  },

  //0、正常util*****************************
  //v1
  // 时间处理方案
  formatTime (date) {
    const year = date.getFullYear()
    let month = date.getMonth() + 1
    let day = date.getDate()
    if(month<10){month="0"+month;}
    if(day<10){day="0"+day;}
    const hour = date.getHours()
    const minute = date.getMinutes()
    const second = date.getSeconds()
    // const t1 = [year, month, day].map(vm.formatNumber).join('/')
    const t1=year+'-'+month+'-'+day;
    return t1;
  },
  //时间戳处理
  add0(m) {
    return m < 10 ? "0" + m : m;
  },
  //1. 到日
  format(shijianchuo) {
    var time = new Date(shijianchuo);
    var y = time.getFullYear();
    var m = time.getMonth() + 1;
    var d = time.getDate();
    var h = time.getHours();
    var mm = time.getMinutes();
    return y + "-" + this.add0(m) + "-" + this.add0(d);
  },
  //2.  到秒
  formatTime(shijianchuo) {
    var time = new Date(shijianchuo);
    var y = time.getFullYear();
    var m = time.getMonth() + 1;
    var d = time.getDate();
    var h = time.getHours();
    var mm = time.getMinutes();
    var ms = time.getSeconds();
    return y + "-" + this.add0(m) + "-" + this.add0(d) + " " + this.add0(h) + ":" +this.add0(mm) + ":" + this.add0(ms);
  },
  //2018-10-8 11:08:05
  //双参数。。(2个循环好像没法统一)
  //遍历2次之后找到需要的title项返回整item
  forName(arr,name){
    for(let i=0;i<arr.length;i++){
      for(let j=0;j<arr[i].length;j++){
        if(arr[i][j].title==name){
          return arr[i][j];
        }
      }
    }
  },
  //替换所有
  replaceAll(str,arg,arg2){
    while(str.indexOf(arg)>=0){
      str=str.replace(arg,arg2);
    }
    return str;
  },

  //1、主干*****************
  //小程序专属
  //获取设备的信息。默认是返回布尔，true是安卓。参数为name时返回name
  sysAct(arg){
    wx.getSystemInfo({
      success: function(res) {
        // console.log(res.brand,res.model,res.platform)
        // vm.phoneName=res.model;
        let phoneName=res.model;
          // brand=true;
        if(arg=="name"){
          return phoneName;
        }else{
          if(res.platform=="android"){
            return true;
          }else{
            return false;
          }
        }
      },
      fail(res){
        console.log('获取设备信息失败 base');
      }
    })
  },
  //modal
  wxMsgLoad(msg,mask){
    wx.showLoading({
      title: msg||'加载中',
      mask:mask||true
    })
  },
  //toast
  wxMsgToast(msg,mask,time){
    wx.showToast({
      title: msg||"添加成功",
      icon: 'none',
      duration: time||2500,
      mask:true,
    })
  },
  wxMsgModal(msg,mask){
    wx.showModal({
      title: '提示',
      content: msg||'这是一个模态弹窗',
      showCancel:false,
    })
  },
  //_________________________________蓝牙
  //关闭蓝牙模块  （主动）
  closeBLTall(){
    let vm=this;
    wx.closeBluetoothAdapter({
      success: function (res) {
        console.log(res);
        wxMsgModal("蓝牙模块已关闭");
      },
      fail(){
        console.log("蓝牙模块关闭失败");
      }
    })
  },
  ActBLE(vms,category,params){
    this.BLEdata.category=category;
    console.log(params);
    //在封装里面处理，undefined就给一个初始
    params?"":params={
      value:"",
      valueNew:"",
      effectData:"",
      effectTimes:"",
      role:vms.$store.state.role,
      superPwd:vms.$store.state.superPwd
    }
    let vm=this,
      mac=vm.BLEdata.deviceId,
      serviceId=vm.BLEdata.serviceId,
      uuid=vm.BLEdata.uuid,
      dlid=vms.$store.state.dlid,
      sendCommand;
    //loading.......
    vm.wxMsgLoad("请求中",true);
    //1、ask server
    function BLEfirstTest(){
      console.log("1、兑换请求码...");
      let $time=new Date(),
        time=vm.formatTime($time);
      // console.log('config ARG >> ',category,dlid," - ",token," - ",mac);
      wx.request({
        method:"POST",
        url: vms.$url2+"api/device/bt_config", 
        data: {
          "access_token":vms.$store.state.rqToken,
          "dlid":dlid,
          "bt_mac":mac,
          "category":category,
          "time_str":time,
          "params":params,
        },
        success: function(res) {
          wx.hideLoading();
          console.log('bt_config RES...',res);
          let rs=res.data.data;
          if(res.data.code==0){
            vm.wxMsgLoad("请求中",true);
            //这里直接保存到当前函数下
            sendCommand=rs.sendCommand;
            vm.BLEdata.commandId=rs.commandId;
            writeBLECharacteristicValue();
          }else{
            // code
            vms.codeMes.data.map((item)=>{
              if(item.code==res.data.rep_code){
                vm.wxMsgToast(item.msg)
              }
            })
            //没有设置超级管理员密码
            if(res.data.code==4007){
              vm.wxMsgToast(res.data.msg);
            }
          }
        },
        fail:function(res){
          console.log("bt_config fail",res);
        }
      })
    }
    //2、action BLE
    function writeBLECharacteristicValue() {
      console.log("2、写入蓝牙设备..");
      sendCommand.map((item,index)=>{
        let num="key_"+(index+1);
        let typedArray = new Uint8Array(item[num].match(/[\da-f]{2}/gi).map(function (h) {
            return parseInt(h, 16)
        }))
        let buffer1 = typedArray.buffer
        console.log('写入参数检查',mac,'-',serviceId,'-',uuid,'-',buffer1);
        wx.writeBLECharacteristicValue({
          deviceId: mac,            //蓝牙设备id
          serviceId: serviceId,     //蓝牙特征值对应服务的 uuid
          characteristicId: uuid,   //蓝牙特征值id
          value: buffer1,           //要写入的二进制的值
          success(res){
            console.log("write to BLE...",res);
          },
          fail(res){
            console.log("write fali...",res);
          },
          complete(res){
            // wx.hideLoading();
          }
        })
      })
    }
    //3、change BLE
    //特征值变化  emit 吧
    BLEfirstTest();
  },
}
export default utils